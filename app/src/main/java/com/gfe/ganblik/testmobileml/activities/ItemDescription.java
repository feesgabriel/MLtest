package com.gfe.ganblik.testmobileml.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.TextView;

import com.gfe.ganblik.testmobileml.R;
import com.gfe.ganblik.testmobileml.helpers.Constanct;

public class ItemDescription extends AppCompatActivity {

    private TextView description;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle(R.string.description);
        setContentView(R.layout.activity_item_description);
        description = findViewById(R.id.description);
        description.setText(getIntent().getStringExtra(Constanct.DESCRIPTION));

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
