package com.gfe.ganblik.testmobileml.models.ItemDescription;

public class Snapshot {
    private String height;

    private String status;

    private String width;

    private String url;

    public String getHeight ()
    {
        return height;
    }

    public String getStatus ()
    {
        return status;
    }

    public String getWidth ()
    {
        return width;
    }

    public String getUrl ()
    {
        return url;
    }
}
