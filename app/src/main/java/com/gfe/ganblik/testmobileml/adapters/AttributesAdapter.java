package com.gfe.ganblik.testmobileml.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.gfe.ganblik.testmobileml.R;
import com.gfe.ganblik.testmobileml.adapters.holders.TitleSubtitleHolder;
import com.gfe.ganblik.testmobileml.models.Attributes;

import java.util.List;

public class AttributesAdapter extends RecyclerView.Adapter<TitleSubtitleHolder> {

    private List<Attributes> itemList;

    public AttributesAdapter(Context context, List<Attributes> itemList ) {
        this.itemList = itemList;
    }

    @Override
    public TitleSubtitleHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.title_subtitle_layout, null);
        TitleSubtitleHolder rcv = new TitleSubtitleHolder(layoutView);
        return rcv;
    }

    @Override
    public void onBindViewHolder(TitleSubtitleHolder holder, int position) {
        final Attributes item = itemList.get(position);
        holder.title.setText(item.getName());
        holder.subtitle.setText(item.getValue_name());

    }

    @Override
    public int getItemCount() {
        return this.itemList.size();
    }
}