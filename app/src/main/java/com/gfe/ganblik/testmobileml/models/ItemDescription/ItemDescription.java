package com.gfe.ganblik.testmobileml.models.ItemDescription;

public class ItemDescription {
    private String text;

    private Snapshot snapshot;

    private String date_created;

    private String last_updated;

    private String plain_text;

    public String getText ()
    {
        return text;
    }

    public Snapshot getSnapshot ()
    {
        return snapshot;
    }


    public String getDate_created ()
    {
        return date_created;
    }

    public String getLast_updated ()
    {
        return last_updated;
    }

    public String getPlain_text ()
    {
        return plain_text;
    }
}
