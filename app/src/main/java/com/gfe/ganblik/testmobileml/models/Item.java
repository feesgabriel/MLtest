package com.gfe.ganblik.testmobileml.models;


import com.gfe.ganblik.testmobileml.models.Details.Review;

public class Item
{
    private String accepts_mercadopago;

    private String original_price;

    private String listing_type_id;

    private String official_store_id;

    private String id;

    private Shipping shipping;

    private String available_quantity;

    private String title;

    private String buying_mode;

    private String[] tags;

    private String currency_id;

    private String condition;

    private String stop_time;

    private String site_id;

    private Seller_address seller_address;

    private String category_id;

    private Seller seller;

    private Installments installments;

    private String price;

    private String thumbnail;

    private String permalink;

    private Address address;

    private Attributes[] attributes;

    private String catalog_product_id;

    private String sold_quantity;

    private Review reviews;

    public Review getReviews() { return reviews; }

    public String getAccepts_mercadopago ()
    {
        return accepts_mercadopago;
    }

    public String getOriginal_price ()
{
    return original_price;
}

    public String getListing_type_id ()
    {
        return listing_type_id;
    }

    public String getId ()
    {
        return id;
    }

    public Shipping getShipping ()
    {
        return shipping;
    }

    public String getAvailable_quantity ()
    {
        return available_quantity;
    }

    public String getTitle ()
    {
        return title;
    }

    public String getBuying_mode ()
    {
        return buying_mode;
    }

    public String[] getTags ()
    {
        return tags;
    }

    public String getCurrency_id ()
    {
        return currency_id;
    }

    public String getCondition ()
    {
        return condition;
    }

    public String getStop_time ()
    {
        return stop_time;
    }

    public String getSite_id ()
    {
        return site_id;
    }

    public Seller_address getSeller_address ()
    {
        return seller_address;
    }

    public String getCategory_id ()
    {
        return category_id;
    }

    public Seller getSeller ()
    {
        return seller;
    }

    public Installments getInstallments ()
    {
        return installments;
    }

    public String getPrice ()
    {
        return price;
    }

    public String getThumbnail ()
    {
        return thumbnail;
    }

    public String getPermalink ()
    {
        return permalink;
    }

    public Address getAddress ()
    {
        return address;
    }

    public Attributes[] getAttributes ()
    {
        return attributes;
    }

    public String getSold_quantity ()
    {
        return sold_quantity;
    }
}

