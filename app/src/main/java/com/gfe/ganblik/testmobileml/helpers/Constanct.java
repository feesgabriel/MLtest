package com.gfe.ganblik.testmobileml.helpers;

public class Constanct {

    public final static String BASE_API = "https://api.mercadolibre.com/";
    public final static String SEARCH_ITEMS_API = BASE_API + "sites/MLA/search?q=";
    public final static String GET_ITEM = BASE_API + "items/";

    public final static String RATING_AVERAGE = "rating_average";
    public final static String RATING_TOTAL = "rating_total";

    public final static String ID = "id";
    public final static String DESCRIPTION = "description";
}
