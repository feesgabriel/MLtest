package com.gfe.ganblik.testmobileml;

import android.app.SearchManager;
import android.app.SearchableInfo;
import android.content.Context;
import android.content.Intent;
import android.provider.SearchRecentSuggestions;
import android.support.constraint.solver.GoalRow;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.gfe.ganblik.testmobileml.activities.ProductPage;
import com.gfe.ganblik.testmobileml.adapters.ResultsAdapter;
import com.gfe.ganblik.testmobileml.contentProviders.MySuggestionProvider;
import com.gfe.ganblik.testmobileml.helpers.Constanct;
import com.gfe.ganblik.testmobileml.interfaces.OnBottomReachedListener;
import com.gfe.ganblik.testmobileml.models.Item;
import com.gfe.ganblik.testmobileml.models.ResultSearch;
import com.gfe.ganblik.testmobileml.networking.CallBackVolley;
import com.gfe.ganblik.testmobileml.networking.CustomRequest;
import com.gfe.ganblik.testmobileml.networking.NetworkController;


import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonParseException;
import android.support.v7.widget.RecyclerView;

public class SearchActivity extends AppCompatActivity {

    private Gson gson;
    private ResultsAdapter rcAdapter;
    private RecyclerView recyclerView;
    private ArrayList<Item> itemListObjects = new ArrayList<>();
    private StaggeredGridLayoutManager staggeredGridLayoutManager;
    private RelativeLayout layoutProgressBar;

    private MenuItem mSearch;
    private int offSet = 0;
    private boolean haveMoreResults = true;
    private String lastQuery = "Android";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        GsonBuilder gsonBuilder = new GsonBuilder();
        gson = gsonBuilder.create();

        recyclerView = findViewById(R.id.recycler_view_Results);
        recyclerView.setHasFixedSize(true);

        staggeredGridLayoutManager = new StaggeredGridLayoutManager(2, 1);
        recyclerView.setLayoutManager(staggeredGridLayoutManager);
        layoutProgressBar = findViewById(R.id.layoutProgressBar);

        rcAdapter = new ResultsAdapter(SearchActivity.this ,itemListObjects , new ResultsAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(Item item) {

                Intent productPageIntent = new Intent(SearchActivity.this, ProductPage.class);
                productPageIntent.putExtra(Constanct.ID,item.getId());
                if ( null != item.getReviews()){
                    productPageIntent.putExtra(Constanct.RATING_AVERAGE,item.getReviews().getReviewAverage());
                    productPageIntent.putExtra(Constanct.RATING_TOTAL,item.getReviews().getReviewTotal());
                }
                startActivity(productPageIntent);

            }
        });
        recyclerView.setAdapter(rcAdapter);

        rcAdapter.setOnBottomReachedListener(new OnBottomReachedListener() {
            @Override
            public void onBottomReached(int position) {
                if (haveMoreResults) {
                    haveMoreResults = false;
                    search(lastQuery);
                }
            }
        });

        newQuery(lastQuery);

    }

    protected void search(String query){

        makeRequest(Constanct.SEARCH_ITEMS_API + query + "&offset="+offSet, new CallBackVolley() {
            @Override
            public void onSuccess(JsonObject result) throws JsonParseException {

                ResultSearch  resultSearch = gson.fromJson( result, ResultSearch.class);
                if (resultSearch.getResults().size() < 50) {
                    haveMoreResults = false;
                }
                else {
                    if (offSet == 0) recyclerView.smoothScrollToPosition(0);
                    offSet = offSet + 50;
                    haveMoreResults = true;
                }

                itemListObjects.addAll(resultSearch.getResults());
                rcAdapter.notifyDataSetChanged();

                if (recyclerView.getVisibility() == View.GONE) showResult();
            }

            @Override
            public void onError(String result) throws Exception {
                Toast.makeText(getApplicationContext(), getString(R.string.try_later),
                        Toast.LENGTH_LONG).show();
                //haveMoreResults = false;
                haveMoreResults = true;
            }
        });
    }

    private void showResult(){

        layoutProgressBar.setVisibility(View.GONE);
        recyclerView.setVisibility(View.VISIBLE);

    }

    private void hideResult(){

        layoutProgressBar.setVisibility(View.VISIBLE);
        recyclerView.setVisibility(View.GONE);

    }


    public void makeRequest(final String url, final CallBackVolley callback) {

        CustomRequest rq = new CustomRequest(Request.Method.GET,
                url, null,
                new Response.Listener<JsonObject>() {
                    @Override
                    public void onResponse(JsonObject response) {
                        try {
                              callback.onSuccess(response);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.v("Response", error.toString());
                        String err = null;
                        if (error instanceof com.android.volley.NoConnectionError){
                            err = "No internet Access!";
                        }
                        try {
                            if(err != "null") {
                                callback.onError(err);
                            }
                            else {
                                callback.onError(error.toString());
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json");
                return headers;
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                return params;
            }

        };
        rq.setPriority(Request.Priority.HIGH);
        NetworkController.getInstance(getApplicationContext()).addToRequestQueue(rq);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_toolbar, menu);
        final SearchManager searchManager=(SearchManager)getSystemService(Context.SEARCH_SERVICE);
        mSearch = menu.findItem(R.id.action_search);
        final SearchView mSearchView = (SearchView) mSearch.getActionView();
        mSearchView.setQueryHint(getString(R.string.Search));

        mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                newQuery(query);
                mSearch.collapseActionView();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
               // mAdapter.getFilter().filter(newText);
                return true;
            }
        });
        mSearchView.setSearchableInfo((searchManager.getSearchableInfo(getComponentName())));
        return super.onCreateOptionsMenu(menu);
    }

    private void newQuery(String query) {
        clearResults();
        hideResult();
        lastQuery = query;
        search(query);
    }

    private void clearResults() {
        lastQuery = "";
        itemListObjects.clear();
        offSet = 0;
        rcAdapter.notifyDataSetChanged();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        setIntent(intent);
        handleSearch(intent);
    }

    public void handleSearch( Intent intent)
    {
        if (Intent.ACTION_SEARCH.equalsIgnoreCase(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);

            SearchRecentSuggestions searchRecentSuggestions=new SearchRecentSuggestions(this,
                    MySuggestionProvider.AUTHORITY,MySuggestionProvider.MODE);

            searchRecentSuggestions.saveRecentQuery(query,null);
            mSearch.collapseActionView();
            newQuery(query);
        }
        else {
            search(lastQuery);
        }
    }
}
