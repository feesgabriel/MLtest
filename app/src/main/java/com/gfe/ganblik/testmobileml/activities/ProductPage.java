package com.gfe.ganblik.testmobileml.activities;

import android.content.Intent;
import android.net.Uri;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.gfe.ganblik.testmobileml.R;
import com.gfe.ganblik.testmobileml.adapters.AttributesAdapter;
import com.gfe.ganblik.testmobileml.adapters.ImagesPagerAdapter;
import com.gfe.ganblik.testmobileml.helpers.Constanct;
import com.gfe.ganblik.testmobileml.models.Details.ItemDetail;
import com.gfe.ganblik.testmobileml.models.Details.Pictures;
import com.gfe.ganblik.testmobileml.models.ItemDescription.ItemDescription;
import com.gfe.ganblik.testmobileml.networking.CallBackVolley;
import com.gfe.ganblik.testmobileml.networking.CustomRequest;
import com.gfe.ganblik.testmobileml.networking.NetworkController;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ProductPage extends AppCompatActivity {

    private Gson gson;
    private TextView item_title_detail, detail_price, sold_quantity, locationValue, description, detail_free_shipping, rating_total, pictures_quantity;
    private ViewPager viewPagerImages;
    private ItemDetail itemDetail;
    private RelativeLayout layoutProgressBar;
    private LinearLayout layoutDescription , attributes_section, description_section, review_layout;
    private ArrayList<Pictures> images = new ArrayList<>();
    private ImagesPagerAdapter imagesAdapter;
    private RatingBar rating_average;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_page);

        initToolbar();
        GsonBuilder gsonBuilder = new GsonBuilder();
        gson = gsonBuilder.create();

        item_title_detail  = findViewById(R.id.item_title_detail);
        detail_price = findViewById(R.id.detail_price);
        detail_free_shipping = findViewById(R.id.detail_free_shipping);
        sold_quantity = findViewById(R.id.sold_quantity);
        locationValue = findViewById(R.id.locationValue);
        description = findViewById(R.id.description);
        description.setOnClickListener(getOnClickDescripton());
        layoutProgressBar = findViewById(R.id.layoutProgressBar);
        layoutDescription = findViewById(R.id.layout_fullDescription);
        review_layout = findViewById(R.id.review_layout);
        rating_total = findViewById(R.id.rating_total);
        rating_average = findViewById(R.id.rating_average);
        pictures_quantity = findViewById(R.id.pictures_quantity);


        attributes_section = findViewById(R.id.attributes_section);
        description_section = findViewById(R.id.description_section);

        fetchItemInfo(getIntent().getStringExtra(Constanct.ID));
        fetchItemDescription(getIntent().getStringExtra(Constanct.ID));
        setRatingItem();
        viewPagerImages = findViewById(R.id.viewPager);

    }

    private void setRatingItem() {
        if (getIntent().hasExtra(Constanct.RATING_AVERAGE) && getIntent().hasExtra(Constanct.RATING_TOTAL)){
            rating_average.setRating(getIntent().getFloatExtra(Constanct.RATING_AVERAGE, 0));
            rating_total.setText(getIntent().getIntExtra(Constanct.RATING_TOTAL, 0) + " " + getString(R.string.opinions));
            review_layout.setVisibility(View.VISIBLE);
            if (rating_total.getText().toString().startsWith("0"))   review_layout.setVisibility(View.GONE);
        }
    }

    private View.OnClickListener getOnClickDescripton() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!description.getText().equals("")){
                    Intent intentDescription =  new Intent(ProductPage.this, com.gfe.ganblik.testmobileml.activities.ItemDescription.class);
                    intentDescription.putExtra(Constanct.DESCRIPTION, description.getText().toString());
                    ProductPage.this.startActivity(intentDescription);
                }
            }
        };
    }


    private void initToolbar() {
        Toolbar mToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        setTitle(getString(R.string.app_name));
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; go home
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void fetchItemInfo(String id) {
        makeRequest(Constanct.GET_ITEM + id, new CallBackVolley() {
            @Override
            public void onSuccess(JsonObject result) throws JsonParseException {

                itemDetail = gson.fromJson( result, ItemDetail.class);
                updateItemInfo(itemDetail);
                showFullDescription();
            }

            @Override
            public void onError(String result) throws Exception {
                Toast.makeText(getApplicationContext(), getString(R.string.try_later),
                        Toast.LENGTH_LONG).show();
            }
        });
    }

    private void showFullDescription() {
        layoutProgressBar.setVisibility(View.GONE);
        layoutDescription.setVisibility(View.VISIBLE);
    }

    public void fetchItemDescription(String id) {

        makeRequest( Constanct.GET_ITEM + id + "/" + Constanct.DESCRIPTION, new CallBackVolley() {
            @Override
            public void onSuccess(JsonObject result) throws JsonParseException {

                ItemDescription itemDescription = gson.fromJson( result, ItemDescription.class);
                updateItemDescription(itemDescription);
            }

            @Override
            public void onError(String result) throws Exception {
                description_section.setVisibility(View.GONE);
            }
        });
    }

    private void updateItemDescription(ItemDescription itemDescription) {
        description.setText(itemDescription.getPlain_text());
    }

    private void updateItemInfo(ItemDetail itemDetail) {

        item_title_detail.setText(itemDetail.getTitle());
        images = itemDetail.getPictures();
        imagesAdapter = new ImagesPagerAdapter(ProductPage.this, images);
        pictures_quantity.setVisibility(View.VISIBLE);
        pictures_quantity.setText(images.size() > 1 ? images.size() + " " + getString(R.string.fotos) : images.size() + " " + getString(R.string.foto));
        viewPagerImages.setAdapter(imagesAdapter);
        imagesAdapter.notifyDataSetChanged();
        detail_price.setText("$ " + itemDetail.getPrice());
        sold_quantity.setText(itemDetail.getSold_quantity()> 0 ? itemDetail.getSold_quantity() +" "+ getString(R.string.sold) : "");
        locationValue.setText(itemDetail.getSeller_address().getState().getName()+ ", "+ itemDetail.getSeller_address().getCity().getName());
        if (itemDetail.getShipping().getFree_shipping()) detail_free_shipping.setText(R.string.free_shipping);

        if (itemDetail.getAttributes().size() >= 0 ) {
            attributes_section.setVisibility(View.VISIBLE);

            RecyclerView recyclerView =  findViewById(R.id.attributes);
            recyclerView.setLayoutManager(new GridLayoutManager(this, 2));
            AttributesAdapter adapter = new AttributesAdapter(this, itemDetail.getAttributes().size() > 10 ? itemDetail.getAttributes().subList(0,10) : itemDetail.getAttributes());
            recyclerView.setAdapter(adapter);
        }

    }

    public void makeRequest(final String url, final CallBackVolley callback) {

        CustomRequest rq = new CustomRequest(Request.Method.GET,
                url, null,
                new Response.Listener<JsonObject>() {
                    @Override
                    public void onResponse(JsonObject response) {
                        try {
                            callback.onSuccess(response);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.v("Response", error.toString());
                        String err = null;
                        if (error instanceof com.android.volley.NoConnectionError){
                            err = "No internet Access!";
                        }
                        try {
                            if(err != "null") {
                                callback.onError(err);
                            }
                            else {
                                callback.onError(error.toString());
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json");
                return headers;
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                return params;
            }

        };
        rq.setPriority(Request.Priority.HIGH);
        NetworkController.getInstance(getApplicationContext()).addToRequestQueue(rq);

    }

    public void buyItem(View v){
        if (itemDetail != null && !itemDetail.getPermalink().equals("")){
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(itemDetail.getPermalink()));
        startActivity(browserIntent);}
    }
}
