package com.gfe.ganblik.testmobileml.models;

public class Seller {
    private String[] tags;

    private String id;

    private String power_seller_status;

    private String real_estate_agency;

    private String car_dealer;

    public String[] getTags ()
    {
        return tags;
    }

    public void setTags (String[] tags)
    {
        this.tags = tags;
    }

    public String getId ()
    {
        return id;
    }

    public String getPower_seller_status ()
    {
        return power_seller_status;
    }

    public String getReal_estate_agency ()
    {
        return real_estate_agency;
    }

    public String getCar_dealer ()
    {
        return car_dealer;
    }

}
