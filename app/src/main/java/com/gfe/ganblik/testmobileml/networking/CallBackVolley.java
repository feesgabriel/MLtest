package com.gfe.ganblik.testmobileml.networking;


import com.google.gson.JsonParseException;
import com.google.gson.JsonObject;

public interface CallBackVolley {
    void onSuccess(JsonObject result) throws JsonParseException;
    void onError(String result) throws Exception;
}
