package com.gfe.ganblik.testmobileml.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class ResultSearch {

    @SerializedName("site_id")
    private String site_id;

    @SerializedName("query")
    private String query;

    @SerializedName("results")
    private ArrayList<Item> results;


    public String getSite_id() {
        return site_id;
    }

    public ArrayList<Item> getResults() {
        return results;
    }

    public String getQuery() {
        return query;
    }


}
