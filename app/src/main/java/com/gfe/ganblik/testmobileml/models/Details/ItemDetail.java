package com.gfe.ganblik.testmobileml.models.Details;

import com.gfe.ganblik.testmobileml.models.Attributes;
import com.gfe.ganblik.testmobileml.models.Seller_address;
import com.gfe.ganblik.testmobileml.models.Shipping;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class ItemDetail {
    private String base_price;

    private String accepts_mercadopago;

    private Object location;

    private Object original_price;

    private String listing_type_id;

    private Descriptions[] descriptions;

    private Object[] variations;

    private Shipping shipping;

    private String available_quantity;

    private Object video_id;

    private String[] coverage_areas;

    private String condition;

    private String stop_time;

    private String status;

    private String category_id;

    private Object parent_item_id;

    private Object health;

    private Object differential_pricing;

    private String thumbnail;

    private String price;

    private String automatic_relist;

    private String secure_thumbnail;

    private String permalink;

    private Object[] sale_terms;

    private String seller_id;

    private Object warranty;

    private String initial_quantity;

    private Object seller_contact;

    private ArrayList<Pictures> pictures;

    private String date_created;

    private Object official_store_id;

    private String id;

    private String title;

    private Geolocation geolocation;

    private Object[] non_mercado_pago_payment_methods;

    private String[] deal_ids;

    private String buying_mode;

    private String[] warnings;

    private String[] tags;

    private String currency_id;

    private String international_delivery_mode;

    private String[] sub_status;

    private Object domain_id;

    private String site_id;

    private Seller_address seller_address;

    private String listing_source;

    private Object subtitle;

    private String start_time;

    private String last_updated;

    private ArrayList<Attributes> attributes;

    private Object catalog_product_id;

    private int sold_quantity;

    public String getBase_price ()
    {
        return base_price;
    }

    public String getAccepts_mercadopago ()
    {
        return accepts_mercadopago;
    }

    public Object getLocation ()
    {
        return location;
    }

    public Object getOriginal_price ()
    {
        return original_price;
    }


    public String getListing_type_id ()
    {
        return listing_type_id;
    }


    public Descriptions[] getDescriptions ()
    {
        return descriptions;
    }

    public Object[] getVariations ()
    {
        return variations;
    }

    public Shipping getShipping ()
    {
        return shipping;
    }

    public String getAvailable_quantity ()
    {
        return available_quantity;
    }

    public Object getVideo_id ()
    {
        return video_id;
    }

    public String[] getCoverage_areas ()
    {
        return coverage_areas;
    }

    public String getCondition ()
    {
        return condition;
    }

    public String getStop_time ()
    {
        return stop_time;
    }

    public String getStatus ()
    {
        return status;
    }

    public String getCategory_id ()
    {
        return category_id;
    }

    public Object getParent_item_id ()
    {
        return parent_item_id;
    }

    public Object getHealth ()
    {
        return health;
    }

    public Object getDifferential_pricing ()
    {
        return differential_pricing;
    }

    public String getThumbnail ()
    {
        return thumbnail;
    }

    public String getPrice ()
    {
        return price;
    }

    public String getAutomatic_relist ()
    {
        return automatic_relist;
    }

    public String getSecure_thumbnail ()
    {
        return secure_thumbnail;
    }

    public String getPermalink ()
    {
        return permalink;
    }

    public Object[] getSale_terms ()
    {
        return sale_terms;
    }

    public String getSeller_id ()
    {
        return seller_id;
    }

    public Object getWarranty ()
    {
        return warranty;
    }

    public String getInitial_quantity ()
    {
        return initial_quantity;
    }

    public Object getSeller_contact ()
    {
        return seller_contact;
    }

    public ArrayList<Pictures> getPictures ()
    {
        return pictures;
    }

    public String getDate_created ()
    {
        return date_created;
    }

    public Object getOfficial_store_id ()
    {
        return official_store_id;
    }

    public String getId ()
    {
        return id;
    }

    public String getTitle ()
    {
        return title;
    }

    public Geolocation getGeolocation ()
    {
        return geolocation;
    }

    public Object[] getNon_mercado_pago_payment_methods ()
    {
        return non_mercado_pago_payment_methods;
    }

    public String[] getDeal_ids ()
    {
        return deal_ids;
    }

    public String getBuying_mode ()
    {
        return buying_mode;
    }

    public String[] getWarnings ()
    {
        return warnings;
    }

    public String[] getTags ()
    {
        return tags;
    }

    public String getCurrency_id ()
    {
        return currency_id;
    }

    public String getInternational_delivery_mode ()
    {
        return international_delivery_mode;
    }

    public String[] getSub_status ()
    {
        return sub_status;
    }

    public Object getDomain_id ()
    {
        return domain_id;
    }

    public String getSite_id ()
    {
        return site_id;
    }

    public Seller_address getSeller_address ()
    {
        return seller_address;
    }

    public String getListing_source ()
    {
        return listing_source;
    }

    public Object getSubtitle ()
    {
        return subtitle;
    }

    public String getStart_time ()
    {
        return start_time;
    }

    public String getLast_updated ()
    {
        return last_updated;
    }

    public ArrayList<Attributes> getAttributes ()
    {
        return attributes;
    }

    public Object getCatalog_product_id ()
    {
        return catalog_product_id;
    }

    public int getSold_quantity ()
    {
        return sold_quantity;
    }
}
