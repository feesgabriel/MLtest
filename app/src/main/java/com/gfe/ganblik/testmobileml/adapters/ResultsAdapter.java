package com.gfe.ganblik.testmobileml.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.gfe.ganblik.testmobileml.R;
import com.gfe.ganblik.testmobileml.adapters.holders.ResultHolder;
import com.gfe.ganblik.testmobileml.interfaces.OnBottomReachedListener;
import com.gfe.ganblik.testmobileml.models.Item;
import com.squareup.picasso.Picasso;

import java.util.List;

public class ResultsAdapter extends RecyclerView.Adapter<ResultHolder> {

        private List<Item> itemList;
        private Context context;
        private OnBottomReachedListener onBottomReachedListener;
        private OnItemClickListener listener;

        public interface OnItemClickListener {
            void onItemClick(Item item);
        }

        public ResultsAdapter(Context context, List<Item> itemList, OnItemClickListener listener ) {
            this.itemList = itemList;
            this.context = context;
            this.listener = listener;
        }

        @Override
        public ResultHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_result, null);
            ResultHolder rcv = new ResultHolder(layoutView);
            return rcv;
        }

        @Override
        public void onBindViewHolder(ResultHolder holder, int position) {
            final Item item = itemList.get(position);
            holder.itemTitle.setText(item.getTitle());
            holder.itemPrecio.setText("$ " +item.getPrice());
            Picasso.get().load((itemList.get(position).getThumbnail()).replace("-I.jpg","-F.jpg")).placeholder(R.drawable.ic_launcher_foreground).into(holder.itemImage);

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View v) {
                    listener.onItemClick(item);
                }
            });

            if (position == itemList.size() - 1) onBottomReachedListener.onBottomReached(position);
            if (item.getShipping().getFree_shipping()) {holder.item_free_shipping.setVisibility(View.VISIBLE);}
              else { holder.item_free_shipping.setVisibility(View.GONE);}
        }

        @Override
        public int getItemCount() {
            return this.itemList.size();
        }

        public void setOnBottomReachedListener(OnBottomReachedListener onBottomReachedListener){
            this.onBottomReachedListener = onBottomReachedListener;
        }
}
