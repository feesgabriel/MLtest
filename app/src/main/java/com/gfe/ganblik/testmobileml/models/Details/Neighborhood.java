package com.gfe.ganblik.testmobileml.models.Details;

public class Neighborhood {
    private String id;

    private String name;

    public String getId ()
    {
        return id;
    }

    public String getName ()
    {
        return name;
    }

}
