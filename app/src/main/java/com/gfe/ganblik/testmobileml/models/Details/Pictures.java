package com.gfe.ganblik.testmobileml.models.Details;

public class Pictures {
    private String id;

    private String quality;

    private String max_size;

    private String url;

    private String size;

    private String secure_url;

    public String getId ()
    {
        return id;
    }

    public String getQuality ()
    {
        return quality;
    }

    public String getMax_size ()
    {
        return max_size;
    }

    public String getUrl ()
    {
        return url;
    }

    public String getSize ()
    {
        return size;
    }

    public String getSecure_url ()
    {
        return secure_url;
    }

}
