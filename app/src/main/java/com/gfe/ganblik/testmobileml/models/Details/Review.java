package com.gfe.ganblik.testmobileml.models.Details;

public class Review {

    private float rating_average;

    private int total;

    public float getReviewAverage ()
    {
        return rating_average;
    }

    public int getReviewTotal ()
    {
        return total;
    }
}
