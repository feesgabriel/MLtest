package com.gfe.ganblik.testmobileml.models;

public class Address {
    private String city_id;

    private String state_id;

    private String city_name;

    private String state_name;

    public String getCity_id ()
    {
        return city_id;
    }

    public String getState_id ()
    {
        return state_id;
    }

    public String getCity_name ()
    {
        return city_name;
    }

    public String getState_name ()
    {
        return state_name;
    }
}
