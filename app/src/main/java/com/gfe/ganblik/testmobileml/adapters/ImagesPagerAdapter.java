package com.gfe.ganblik.testmobileml.adapters;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.gfe.ganblik.testmobileml.R;
import com.gfe.ganblik.testmobileml.models.Details.Pictures;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class ImagesPagerAdapter extends PagerAdapter {
    Context context;
    ArrayList<Pictures> images;
    LayoutInflater layoutInflater;


    public ImagesPagerAdapter(Context context, ArrayList<Pictures> images) {
        this.context = context;
        this.images = images;
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return images.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view ==  object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        View itemView = layoutInflater.inflate(R.layout.image_item, container, false);

        ImageView imageView = itemView.findViewById(R.id.imageView_item);
        Picasso.get().load(images.get(position).getUrl()).placeholder(R.drawable.ic_launcher_foreground).into(imageView);

        container.addView(itemView);

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(context, "you clicked image " + (position + 1), Toast.LENGTH_LONG).show();
            }
        });

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }
}
