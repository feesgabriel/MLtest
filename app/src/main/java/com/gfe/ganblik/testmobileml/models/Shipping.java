package com.gfe.ganblik.testmobileml.models;

public class Shipping {
    private String[] tags;

    private boolean free_shipping;

    private String logistic_type;

    private String mode;

    public String[] getTags ()
    {
        return tags;
    }

    public boolean getFree_shipping ()
    {
        return free_shipping;
    }

    public String getLogistic_type ()
    {
        return logistic_type;
    }

    public String getMode ()
    {
        return mode;
    }

}
