package com.gfe.ganblik.testmobileml.models;

public class Attributes  {
        private String id;

        private String attribute_group_id;

        private String name;

        private String value_name;

        private String value_id;

        private String attribute_group_name;

        private Object value_struct;

        public String getId ()
        {
            return id;
        }

        public String getAttribute_group_id ()
        {
            return attribute_group_id;
        }

        public String getName ()
        {
            return name;
        }

        public String getValue_name ()
        {
            return value_name;
        }

        public String getValue_id ()
        {
            return value_id;
        }

        public String getAttribute_group_name ()
        {
            return attribute_group_name;
        }

        public Object getValue_struct ()
        {
            return value_struct;
        }
    }


