package com.gfe.ganblik.testmobileml.adapters.holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.gfe.ganblik.testmobileml.R;


public class ResultHolder extends RecyclerView.ViewHolder {

    public TextView itemTitle, itemPrecio;
    public ImageView item_free_shipping;
    public ImageView itemImage;


    public ResultHolder(View itemView) {
        super(itemView);
        itemTitle = itemView.findViewById(R.id.item_title);
        itemImage =  itemView.findViewById(R.id.item_image);
        itemPrecio = itemView.findViewById(R.id.item_precio);
        item_free_shipping = itemView.findViewById(R.id.item_free_shipping);
    }
}