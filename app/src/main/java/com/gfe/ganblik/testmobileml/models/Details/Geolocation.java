package com.gfe.ganblik.testmobileml.models.Details;

public class Geolocation {
    private String longitude;

    private String latitude;

    public String getLongitude ()
    {
        return longitude;
    }

    public String getLatitude ()
    {
        return latitude;
    }
}
