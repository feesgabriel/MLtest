package com.gfe.ganblik.testmobileml.models.Details;

import com.gfe.ganblik.testmobileml.models.City;
import com.gfe.ganblik.testmobileml.models.State;

public class SearchLocation {
    private State state;

    private Neighborhood neighborhood;

    private City city;

    public State getState ()
    {
        return state;
    }

    public Neighborhood getNeighborhood ()
    {
        return neighborhood;
    }

    public City getCity ()
    {
        return city;
    }


}
