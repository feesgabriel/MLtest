package com.gfe.ganblik.testmobileml.interfaces;

public interface OnBottomReachedListener {

    void onBottomReached(int position);

}